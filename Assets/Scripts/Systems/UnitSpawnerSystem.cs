using Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace Systems
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class UnitSpawnerSystem : JobComponentSystem
    {
        BeginInitializationEntityCommandBufferSystem _mEntityCommandBufferSystem;

        protected override void OnCreate()
        {
            _mEntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
        }

        struct SpawnJob : IJobForEachWithEntity<UnitSpawner, LocalToWorld>
        {
            public EntityCommandBuffer CommandBuffer;

            public void Execute(Entity entity, int index, [ReadOnly] ref UnitSpawner spawner,
                [ReadOnly] ref LocalToWorld location)
            {
                var dest = new float3(0f,0f,0f);
                
                for (var x = 0; x < spawner.CountX; x++)
                {
                    for (var y = 0; y < spawner.CountY; y++)
                    {
                        var instance = CommandBuffer.Instantiate(spawner.Prefab);

                        var position = math.transform(location.Value,
                            new float3(x * 2, 0, y * 2));

                        CommandBuffer.SetComponent(instance, new Translation { Value = position });
                        CommandBuffer.SetComponent(instance, new Rotation {Value = quaternion.identity});
                        
                        CommandBuffer.AddComponent(instance, new UnitNavAgent() {AgentStatus = NavAgentStatus.Moving, FinalDestination = dest});
                    }
                }

                CommandBuffer.DestroyEntity(entity);
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new SpawnJob
            {
                CommandBuffer = _mEntityCommandBufferSystem.CreateCommandBuffer()
            }.ScheduleSingle(this, inputDeps);
            _mEntityCommandBufferSystem.AddJobHandleForProducer(job);

            return job;
        }
    }
}

