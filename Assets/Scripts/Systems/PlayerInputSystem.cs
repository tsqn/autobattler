using Components;
using Extensions;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Systems
{
    public class PlayerInputSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref PlayerInputComponent input) =>
            {
                input.LeftClick = Input.GetMouseButtonDown(0);
                input.RightClick = Input.GetMouseButtonDown(1);

                var mousePos = Input.mousePosition;
                var ray = Camera.main.ScreenPointToRay(mousePos);
                if (Physics.Raycast(ray, out var hit))
                    if (hit.collider != null)
                        mousePos = new float3(hit.point.x, 0, hit.point.z);

                input.MousePosition = mousePos.ToFloat3();
            });
        }
    }
}