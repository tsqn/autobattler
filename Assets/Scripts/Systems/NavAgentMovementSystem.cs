using Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Systems
{
    public class NavAgentMovementSystem : JobComponentSystem
    {
        public struct NavAgentMovementJob : IJobForEach<Translation, UnitNavAgent>
        {
            public float DT;

            public void Execute(ref Translation position, [ReadOnly] ref UnitNavAgent agent)
            {
                var distance = math.distance(agent.FinalDestination, position.Value);
                var direction = math.normalize(agent.FinalDestination - position.Value);
                float speed = 5;
                if(!(distance < 0.5) && agent.AgentStatus == NavAgentStatus.Moving)
                {
                    position.Value += direction * speed * DT;
                }
            }
        }


        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new NavAgentMovementJob
            {
                DT = Time.deltaTime,
            };
            return job.Schedule(this, inputDeps);
        }
    }
}