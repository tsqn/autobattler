using Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Systems
{
    namespace Systems
    {
        public class NavAgentRotationSystem : JobComponentSystem
        {
            protected override JobHandle OnUpdate(JobHandle inputDeps)
            {
                var job = new NavAgentRotationJob();

                return job.Schedule(this, inputDeps);
            }

            public static quaternion LookAt(float3 sourcePoint, float3 destPoint)
            {
                var forwardVector = Vector3.Normalize(destPoint - sourcePoint);

                var dot = Vector3.Dot(Vector3.forward, forwardVector);

                if (math.abs(dot - -1.0f) < 0.000001f)
                    return new quaternion(Vector3.up.x, Vector3.up.y, Vector3.up.z, 3.1415926535897932f);
                if (math.abs(dot - 1.0f) < 0.000001f) return quaternion.identity;

                var rotAngle = math.acos(dot);
                var rotAxis = Vector3.Cross(Vector3.forward, forwardVector);
                rotAxis = Vector3.Normalize(rotAxis);
                return CreateFromAxisAngle(rotAxis, rotAngle);
            }

            public static quaternion CreateFromAxisAngle(float3 axis, float angle)
            {
                var halfAngle = angle * .5f;
                var s = math.sin(halfAngle);
                quaternion q;

                q.value = new float4(axis.x * s, axis.y * s, axis.z * s, math.cos(halfAngle));
                return q;
            }

            public struct NavAgentRotationJob : IJobForEach<Rotation, Translation, UnitNavAgent>
            {
                public void Execute(ref Rotation rotation, ref Translation position, [ReadOnly] ref UnitNavAgent agent)
                {
                    rotation.Value = LookAt(position.Value, agent.FinalDestination);
                }
            }
        }
    }
}