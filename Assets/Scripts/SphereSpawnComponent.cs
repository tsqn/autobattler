using System;
using Unity.Entities;
using UnityEngine.Serialization;

[Serializable]
public struct SphereSpawnComponent : ISharedComponentData
{
    [FormerlySerializedAs("Prefab")] public Entity prefab;
    [FormerlySerializedAs("Radius")] public float radius;
    [FormerlySerializedAs("Count")] public int count;
}