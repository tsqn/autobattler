﻿using Components;
using Extensions;
using Unity.Collections;
using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Serialization;

public class EntitySpawner : MonoBehaviour
{
    [FormerlySerializedAs("_count")] [SerializeField] private int count;
    [FormerlySerializedAs("_destination")] [SerializeField] private GameObject destination;
    [FormerlySerializedAs("_material")] [SerializeField] private Material material;
    [FormerlySerializedAs("_mesh")] [SerializeField] private Mesh mesh;
    [FormerlySerializedAs("_speed")] [SerializeField] private float speed;

    private void Start()
    {
        var entityManager = World.Active.EntityManager;
        var archytype = entityManager.CreateArchetype(
            typeof(MoveSpeedComponent),
            typeof(Translation),
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(UnitNavAgent)
        );

        entityManager.CreateEntity(typeof(PlayerInputComponent));


        var entityArray = new NativeArray<Entity>(count, Allocator.Temp);
        entityManager.CreateEntity(archytype, entityArray);

        for (var i = 0; i < entityArray.Length; i++)
        {
            var entity = entityArray[i];

            entityManager.SetComponentData(entity,
                new UnitNavAgent
                {
                    FinalDestination = destination.transform.position.ToFloat3(),
                    AgentStatus = NavAgentStatus.Moving
                });

            entityManager.SetComponentData(entity, new MoveSpeedComponent {Speed = speed});

            var position = new Translation {Value = transform.position + Random.Range(1f, 2f) * Random.onUnitSphere};

            entityManager.SetComponentData(entity,
                position);

            entityManager.SetSharedComponentData(entity, new RenderMesh
            {
                material = material,
                mesh = mesh
            });
        }

        entityArray.Dispose();
    }
}