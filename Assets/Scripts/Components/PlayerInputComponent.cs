using Unity.Entities;
using Unity.Mathematics;

namespace Components
{
    public struct PlayerInputComponent : IComponentData
    {
        public BlittableBool LeftClick;
        public BlittableBool RightClick;
        public float3 MousePosition;
    }
}