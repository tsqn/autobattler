namespace Components
{
    public class UnitStatusComponent
    {
        public UnitStatus Status;
    }

    public enum UnitStatus
    {
        Idle,
        Move,
        Fight
    }
}