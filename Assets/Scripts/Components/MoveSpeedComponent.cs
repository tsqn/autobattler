using Unity.Entities;

namespace Components
{
    public struct MoveSpeedComponent : IComponentData
    {
        public float Speed;
    }
}