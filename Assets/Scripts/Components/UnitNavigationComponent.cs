using Unity.Entities;
using Unity.Mathematics;

namespace Components
{
    public enum NavAgentStatus
    {
        Idle = 0,
        Moving = 1,
    }


    public struct UnitNavAgent : IComponentData
    {
        public float3 FinalDestination;
        public NavAgentStatus AgentStatus;
    }
}